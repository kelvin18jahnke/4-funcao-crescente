<?php
require_once './funcoes/sequencia.php';

$sequencia = new sequencia();

if (isset($_POST['valor'])) {

    $valor = $_POST['valor'];

    if ($valor == '') {
        echo '<div class="alert alert-info" role="alert">
                Informe uma sequência de números!
                </div>';
    } else {

        //Cria o array
        $arrayValor = array();
        $verificaSeque = str_replace(',','', $valor);
        
        If (strlen($verificaSeque) > 3) {
            //Transforma a string em array
            $arrayValor = explode(',', $valor);
          
             //Funnção que retorna se é crescente o array
            $numeroSequencia = $sequencia->SequenciaCrescente($arrayValor);
           
        }else{
            echo '<div class="alert alert-info" role="alert">
                Informe uma sequência de números!
                </div>';
        }
     
    }
  
} else {
    $valor = '';
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
        <title>Área:</title>
    </head>
    <body>

        <form action="index.php" method="POST">
            <div class="row g-3 align-items-center" style="margin: 2% 10%;">
                <h1>Função que Retorna se a sequência pode ser crescente</h1>
                <div class="col-auto">
                    <label  class="col-form-label">Informe uma sequencia de números inteiros sepando cada numero por virgula:</label>
                </div>
                <div class="col-auto">
                    <input type="text" name="valor" value="<?= $valor ?>" class="form-control">
                </div>
                <div class="col-auto">
                    <span  class="form-text">
                        <?php
                        if (isset($numeroSequencia)) {
                            if ($numeroSequencia && true) {
                                echo 'É uma sequência crescente.';
                            } else {
                                echo 'Não é uma sequência crescente.';
                            }
                        }
                        ?>
                    </span>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </form>
    </body>
</html>
